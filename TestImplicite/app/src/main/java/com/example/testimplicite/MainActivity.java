package com.example.testimplicite;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Pair;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.regex.Pattern;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void onClickedButton(View view){
        int id = view.getId();
        String telephoneNumber;
        String latitude = "";
        String longitude ="";
        String url;
        Uri uri;
        Intent it;
        switch (id){
            case R.id.smsButton:
                telephoneNumber = setNumberPhone();
                if(checkSmsMmsNumber(telephoneNumber)) {
                    valideNumber(View.INVISIBLE);
                    TextView textView = findViewById(R.id.smsMmsNumberError);
                    textView.setVisibility(View.INVISIBLE);
                    uri = Uri.parse("smsto:" + telephoneNumber);
                    it = new Intent(Intent.ACTION_SENDTO, uri);
                    it.putExtra("sms_body", "J'envoie un sms depuis mon application :)");
                    startActivity(it);

                }else{
                    valideNumber(View.VISIBLE);
                }
                break;
            case R.id.mmsButton:
                telephoneNumber = setNumberPhone();
                if(checkSmsMmsNumber(telephoneNumber)) {
                    valideNumber(View.INVISIBLE);
                    Uri uriImage = Uri.parse("content://media/external/images/media/1");
                    uri = Uri.parse("mmsto:" + telephoneNumber);
                    it = new Intent(Intent.ACTION_SENDTO, uri);
                    //pour charger une image dans le mms
                    it.putExtra(Intent.EXTRA_STREAM, uriImage);
                    // contenu du message
                    it.putExtra("sms_body", "J'envoie un mms depuis mon application :)");
                    startActivity(it);
                }
                else{
                    valideNumber(View.VISIBLE);
                }
                break;
            case R.id.appelButton:
                telephoneNumber = setNumberPhone();
                if(checkCallNumber(telephoneNumber)) {
                    valideNumber(View.INVISIBLE);
                    uri = Uri.parse("tel:" + telephoneNumber);
                    it = new Intent(Intent.ACTION_DIAL, uri);
                    startActivity(it);
                }
                else{
                    valideNumber(View.VISIBLE);
                }
                break;
            case R.id.webButton:
                url = setUrl();
                if(checkValidUrl(url)) {
                    valideUrl(View.INVISIBLE);
                    uri = Uri.parse(url);
                    it = new Intent(Intent.ACTION_VIEW, uri);
                    startActivity(it);
                }else{
                    valideUrl(View.VISIBLE);
                }
                break;
            case R.id.mapButton:
                latitude = setLatitude();
                longitude = setLongitude();
                if(checkLocalisation(latitude,longitude)) {
                    valideGps(View.INVISIBLE);
                    uri = Uri.parse("geo:" + latitude + "," + longitude);
                    it = new Intent(Intent.ACTION_VIEW, uri);
                    startActivity(it);
                }
                else {
                    valideGps(View.VISIBLE);
                }
                break;
        }
    }

    public String setUrl(){
        EditText editText = findViewById(R.id.urlEditText);
        return editText.getText().toString().trim() ;
    }
    public String setLatitude(){
        EditText latitudeText = findViewById(R.id.latitudeEditText);
        return latitudeText.getText().toString();

    }
    public String setLongitude(){
        EditText longitudeText = findViewById(R.id.longitudeEditText);
        return longitudeText.getText().toString();
    }

    public void valideGps(int isVisible){
        TextView textView = findViewById(R.id.gpsError);
        textView.setVisibility(isVisible);
    }
    public void valideUrl(int isVisible){
        TextView textView = findViewById(R.id.urlError);
        textView.setVisibility(isVisible);
    }
    public boolean checkValidUrl(String url){
        return Pattern.matches("https?:\\/\\/(www\\.)?[-a-zA-Z0-9@:%._\\+~#=]" +
                "{1,256}\\.[a-zA-Z0-9()]{1,6}\\b([-a-zA-Z0-9()@:%_\\+.~#?&//=]*)",url);
    }
    public void valideNumber(int isVisible){
        TextView textView = findViewById(R.id.smsMmsNumberError);
        textView.setVisibility(isVisible);
    }

    public Boolean checkLocalisation(String latitude,String longitude){
        if(latitude.isEmpty() || longitude.isEmpty()) return false;
        double longitudeDouble = Double.parseDouble(longitude);
        double latitudeDouble = Double.parseDouble(latitude);
        return Pattern.matches("-?[0-9]+(\\.[0-9]+)?,-?[0-9]+(\\.[0-9]+)?",latitude+","+longitude)
                && (longitudeDouble >= -180 && longitudeDouble <= 180)
                && (latitudeDouble >=-90 && latitudeDouble <=90);
    }
    public Boolean checkCallNumber(String number){
        return (Pattern.matches("0[1-9][0-9]{8}",number) && number.length()==10);
    }

    public Boolean checkSmsMmsNumber(String number){
        return (number.startsWith("06") || number.startsWith("07")) && number.length()==10 && Pattern.matches("[0-9]+",number);
    }
    public String setNumberPhone(){
        EditText editText = findViewById(R.id.numeroEditText);
        return editText.getText().toString();

    }
}