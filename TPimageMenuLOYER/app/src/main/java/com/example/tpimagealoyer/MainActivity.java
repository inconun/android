package com.example.tpimagealoyer;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.FileNotFoundException;


public class MainActivity extends AppCompatActivity {
    private final String GRIS_MIN_MAX = "GRIS_MIN_MAX";
    private final String GRIS_FLOTTANT = "GRIS_FLOTTANT";
    private final String INVERSER = "INVERSER_COULEURS";
    private final String GRIS = "GRIS_CLASSIQUE";


    public final int MY_PERMISSIONS_REQUEST_STORAGE = 1;
    private ImageView imageView;
    private Bitmap imageViewCopy;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        imageView = findViewById(R.id.imageLoaded);
        registerForContextMenu(imageView);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_activity_context_menu, menu);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_activity_menu, menu);
        return true;
    }


    @Override
    public boolean onContextItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.inverser_couleurs_context_menu:
                inverserCouleurs();
                return true;
            case R.id.gris_couleur_context_menu:
                niveauGrisCouleurs();
                return true;
            case R.id.gris_min_max_context_menu:
                grisMinMax();
                return true;
            case R.id.gris_flottant_context_menu:
                grisFlottant();
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }

    public void doFilter(String type) {
        Bitmap bitmap = ((BitmapDrawable) imageView.getDrawable()).getBitmap();

        Bitmap bitmapChild = bitmap.copy(bitmap.getConfig(), true);
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        int pixel;
        int red;
        int blue;
        int green;
        int rgb = 0;
        int colorMean;
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                pixel = bitmapChild.getPixel(i, j);
                red = Color.red(pixel);
                blue = Color.blue(pixel);
                green = Color.green(pixel);
                switch (type) {
                    case INVERSER:
                        rgb = Color.rgb(255 - red, 255 - green, 255 - blue);
                        break;
                    case GRIS_MIN_MAX:
                        colorMean = (int) (Math.max(red, Math.max(blue, green)) + Math.min(red, Math.min(blue, green)) / 2.0);
                        rgb = Color.rgb(colorMean, colorMean, colorMean);
                        break;
                    case GRIS_FLOTTANT:
                        colorMean = (int) (0.21 * red + 0.72 * green + 0.07 * blue);
                        rgb = Color.rgb(colorMean, colorMean, colorMean);
                        break;
                    case GRIS:
                        colorMean = (red + blue + green) / 3;
                        rgb = Color.rgb(colorMean, colorMean, colorMean);
                        break;
                }
                bitmapChild.setPixel(i, j, rgb);
            }
        }
        imageView.setImageBitmap(bitmapChild);
    }

    public void rotationHoraire(MenuItem menuItem) {
        imageView.setRotation(imageView.getRotation() + 90);
    }

    public void rotationAntiHoraire(MenuItem menuItem) {
        imageView.setRotation(imageView.getRotation() - 90);
    }


    public void grisMinMax() {
        if (imageView.getDrawable() instanceof BitmapDrawable) {
            doFilter(GRIS_MIN_MAX);
        }
    }

    public void grisFlottant() {
        if (imageView.getDrawable() instanceof BitmapDrawable) {
            doFilter(GRIS_FLOTTANT);
        }
    }

    public void inverserCouleurs() {

        if (imageView.getDrawable() instanceof BitmapDrawable) {
            doFilter(INVERSER);
        }
    }

    public void niveauGrisCouleurs() {

        if (imageView.getDrawable() instanceof BitmapDrawable) {
            doFilter(GRIS);
        }
    }

    public void loadImage(View view) throws FileNotFoundException {

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                    MY_PERMISSIONS_REQUEST_STORAGE);
        } else {
            Intent it = new Intent(Intent.ACTION_GET_CONTENT);
            it.setType("image/*");
            startActivityForResult(Intent.createChooser(it, "Select Picture"), 1);
        }

    }

    public void restaureImage(View view) {
        imageView.setImageBitmap(imageViewCopy);
        imageView.setRotation(0);
    }

    public void verticalMirror(MenuItem menuItem) {
        // parcourir le bit map , faire boucle
        ImageView image = findViewById(R.id.imageLoaded);
        if (image.getDrawable() instanceof BitmapDrawable) {
            Bitmap bitmap = ((BitmapDrawable) image.getDrawable()).getBitmap();

            Bitmap bitmapChild = bitmap.copy(bitmap.getConfig(), true);
            int width = bitmap.getWidth();
            int height = bitmap.getHeight();
            for (int i = 0; i < width; i++) {
                for (int j = 0; j < height; j++) {
                    bitmapChild.setPixel(i, j, bitmap.getPixel(i, height - 1 - j));
                }
            }
            image.setImageBitmap(bitmapChild);
        }
    }

    public void horizontalMirror(MenuItem menuItem) {
        // parcourir le bit map , faire boucle
        ImageView image = findViewById(R.id.imageLoaded);
        if (image.getDrawable() instanceof BitmapDrawable) {
            Bitmap bitmap = ((BitmapDrawable) image.getDrawable()).getBitmap();

            Bitmap bitmapChild = bitmap.copy(bitmap.getConfig(), true);
            int width = bitmap.getWidth();
            int height = bitmap.getHeight();
            for (int i = 0; i < width; i++) {
                for (int j = 0; j < height; j++) {
                    bitmapChild.setPixel(i, j, bitmap.getPixel(width - 1 - i, j));
                }
            }
            image.setImageBitmap(bitmapChild);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == 1) {
                Uri selectedImageUri = data.getData();

                // ----- pr´eparer les options de chargement de l’image
                BitmapFactory.Options option = new BitmapFactory.Options();
                option.inMutable = true; // l’image pourra être modifiée
                // ------ chargement de l’image - valeur retourn´ee null en cas d’erreur
                try {
                    Bitmap bm = BitmapFactory.decodeStream(getContentResolver().openInputStream(selectedImageUri), null, option);
                    ImageView view = findViewById(R.id.imageLoaded);
                    view.setImageBitmap(bm);
                    TextView textView = findViewById(R.id.urlView);
                    textView.setText(selectedImageUri.getPath());
                    imageViewCopy = bm;
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}