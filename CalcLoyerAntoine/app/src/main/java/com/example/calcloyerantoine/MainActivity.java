package com.example.calcloyerantoine;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;

import org.w3c.dom.Text;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void exitApp(View maVue){
        finishAndRemoveTask();
    }

    public void reset(View maVue){
      EditText editText = (EditText) findViewById(R.id.first_val);
      editText.setText("");
      editText = (EditText) findViewById(R.id.second_val);
      editText.setText("");

      TextView textView = (TextView) findViewById(R.id.res);
      textView.setText(R.string.resultat);
    }

    public void makeCalc(View maVue){
        RadioGroup radioGroup = (RadioGroup) findViewById(R.id.rad_group);
        TextView textView = (TextView) findViewById(R.id.res);

        EditText editText = (EditText) findViewById(R.id.first_val);
        EditText editText2 = (EditText) findViewById(R.id.second_val);

        if(editText.getText().toString().trim().length() > 0 && editText2.getText().toString().trim().length() > 0 ) {
            double first_val = Double.parseDouble(editText.getText().toString());
            double second_val = Double.parseDouble(editText2.getText().toString());

            int selectedValue = radioGroup.getCheckedRadioButtonId();
            double res;
            switch (selectedValue) {
                case R.id.rad_plus:
                    res = first_val + second_val;
                    textView.setText(Double.toString(res));
                    break;
                case R.id.rad_moins:
                    res = first_val - second_val;
                    textView.setText(Double.toString(res));
                    break;
                case R.id.rad_mult:
                    res = first_val * second_val;
                    textView.setText(Double.toString(res));
                    break;
                case R.id.rad_div:
                    if(second_val==0)textView.setText("Division par 0 impossible ! ");
                    else {
                        res = first_val / second_val;
                        textView.setText(Double.toString(res));
                    }
                    break;
                default:
                    break;
            }
        }
    }
}