package com.example.tpimagealoyer;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.FileNotFoundException;

public class MainActivity extends AppCompatActivity {

    public final int MY_PERMISSIONS_REQUEST_STORAGE=1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void loadImage(View view) throws FileNotFoundException {

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                    MY_PERMISSIONS_REQUEST_STORAGE);
        }else{
            Intent it = new Intent(Intent.ACTION_GET_CONTENT);
            it.setType("image/*");
            startActivityForResult(Intent.createChooser(it,"Select Picture"),1);
        }

    }

    public void verticalMirror(View view){
        // parcourir le bit map , faire boucle
        ImageView image = findViewById(R.id.imageLoaded);
        if(image.getDrawable() instanceof BitmapDrawable) {
            Bitmap bitmap = ((BitmapDrawable) image.getDrawable()).getBitmap();

            Bitmap bitmapChild = bitmap.copy(bitmap.getConfig(),true);
            int width = bitmap.getWidth();
            int height = bitmap.getHeight();
            for(int i =0;i<width;i++){
                for(int j=0;j<height;j++){
                    bitmapChild.setPixel(i,j,bitmap.getPixel(i,height-1-j));
                }
            }
            image.setImageBitmap(bitmapChild);
        }
    }

    public void horizontalMirror(View view){
        // parcourir le bit map , faire boucle
        ImageView image = findViewById(R.id.imageLoaded);
        if(image.getDrawable() instanceof BitmapDrawable) {
            Bitmap bitmap = ((BitmapDrawable) image.getDrawable()).getBitmap();

            Bitmap bitmapChild = bitmap.copy(bitmap.getConfig(),true);
            int width = bitmap.getWidth();
            int height = bitmap.getHeight();
            for(int i =0;i<width;i++){
                for(int j=0;j<height;j++){
                    bitmapChild.setPixel(i,j,bitmap.getPixel(width-1-i,j));
                }
            }
            image.setImageBitmap(bitmapChild);
        }
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == RESULT_OK){
            if (requestCode == 1) {
                Uri selectedImageUri = data.getData();

                // ----- pr´eparer les options de chargement de l’image
                BitmapFactory.Options option = new BitmapFactory.Options();
                option.inMutable = true; // l’image pourra être modifiée
                // ------ chargement de l’image - valeur retourn´ee null en cas d’erreur
                try {
                    Bitmap bm = BitmapFactory.decodeStream(getContentResolver().openInputStream(selectedImageUri), null, option);
                    ImageView view = findViewById(R.id.imageLoaded);
                    view.setImageBitmap(bm);
                    TextView textView = findViewById(R.id.urlView);
                    textView.setText(selectedImageUri.getPath());
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            }
                //selectedImagePath = getPath(selectedImageUri);
            }

    }
}