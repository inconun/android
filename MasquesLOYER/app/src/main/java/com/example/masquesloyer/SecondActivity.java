package com.example.masquesloyer;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class SecondActivity extends AppCompatActivity {

    private EditText prenom;
    private EditText nom;
    private EditText telephone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        prenom = findViewById(R.id.prenom_editText);
        nom = findViewById(R.id.nom_editText);
        telephone = findViewById(R.id.telephone_editText);

        Intent intention = getIntent();

        String prenomToUpdate = intention.getStringExtra("prenom");
        String nomToUpdate = intention.getStringExtra("nom");
        String telephoneToUpdate = intention.getStringExtra("telephone");

        if (prenomToUpdate.length()==0)prenom.setText("saisie vide");
        else prenom.setText(prenomToUpdate);

        if (nomToUpdate.length()==0)nom.setText("saisie vide");
        else nom.setText(nomToUpdate);

        if (telephoneToUpdate.length()==0)telephone.setText("saisie vide");
        else telephone.setText(telephoneToUpdate);


    }


    public void cancel(View view){
        Intent resultat = new Intent();
        setResult(RESULT_CANCELED,resultat);
        finish();
    }

    public void validate(View view){
        Intent resultat = new Intent();
        resultat.putExtra("prenom",prenom.getText().toString());
        resultat.putExtra("nom",nom.getText().toString());
        resultat.putExtra("telephone",telephone.getText().toString());

        setResult(RESULT_OK,resultat);
        finish();
    }
}