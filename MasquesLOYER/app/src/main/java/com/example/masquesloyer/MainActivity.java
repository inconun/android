package com.example.masquesloyer;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private TextView prenom;
    private TextView nom;
    private TextView telephone;

    private TextView numero;
    private TextView rue;
    private TextView ville;
    private TextView codePostal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        prenom = findViewById(R.id.prenom_textView);
        nom = findViewById(R.id.nom_textView);
        telephone = findViewById(R.id.telephone_textView);
        numero = findViewById(R.id.numero_textView);
        rue = findViewById(R.id.rue_textView);
        ville = findViewById(R.id.ville_textView);
        codePostal = findViewById(R.id.code_postal_textView);


        // POUR REMETTRE LES VALEURS DANS LES TEXTVIEW

        if(savedInstanceState!=null){
            prenom.setText(savedInstanceState.getString("prenom"));
            nom.setText(savedInstanceState.getString("nom"));
            telephone.setText(savedInstanceState.getString("telephone"));
            numero.setText(savedInstanceState.getString("numero"));
            rue.setText(savedInstanceState.getString("rue"));
            codePostal.setText(savedInstanceState.getString("codePostal"));
            ville.setText(savedInstanceState.getString("ville"));
        }


    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        // QUAND ON CHANGE ORIENTATION ACTIVITE EST RECREE DONC ON SAVE NOS DONNEES
        super.onSaveInstanceState(outState);
        outState.putString("prenom", prenom.getText().toString());
        outState.putString("nom", nom.getText().toString());
        outState.putString("telephone", telephone.getText().toString());
        outState.putString("numero", numero.getText().toString());
        outState.putString("rue", rue.getText().toString());
        outState.putString("codePostal", codePostal.getText().toString());
        outState.putString("ville", ville.getText().toString());
    }

    public void updateSuperieur(View view){


        Intent intention = new Intent(this,SecondActivity.class);
        intention.putExtra("prenom",prenom.getText().toString());
        intention.putExtra("nom",nom.getText().toString());
        intention.putExtra("telephone",telephone.getText().toString());

        startActivityForResult(intention,1);
    }

    public void updateInferieur(View view){

        Intent intention = new Intent(this,ThirdActivity.class);
        intention.putExtra("numero",numero.getText().toString());
        intention.putExtra("rue",rue.getText().toString());
        intention.putExtra("codePostal",codePostal.getText().toString());
        intention.putExtra("ville",ville.getText().toString());

        startActivityForResult(intention,2);
    }

    protected void onActivityResult(int RequestCode,int ResultCode,Intent data) {

        super.onActivityResult(RequestCode, ResultCode, data);
        if(ResultCode!=RESULT_CANCELED) {
            switch (RequestCode) {
                case 1: // on a modifié la partie supérieure
                    // peut être verifier les chaines vides ?
                    // SURTOUT FAIRE POUR QUE LES CHAMPS NE SE CHEVAUCHENT PAS QUAND ON MET UN TEXTE TROP LONG
                    prenom.setText(data.getStringExtra("prenom"));
                    nom.setText(data.getStringExtra("nom"));
                    telephone.setText(data.getStringExtra("telephone"));
                    break;
                case 2:
                    numero.setText(data.getStringExtra("numero"));
                    rue.setText(data.getStringExtra("rue"));
                    codePostal.setText(data.getStringExtra("code"));
                    ville.setText(data.getStringExtra("ville"));
                default:
                    System.out.println("case default");
            }
        }
    }
}