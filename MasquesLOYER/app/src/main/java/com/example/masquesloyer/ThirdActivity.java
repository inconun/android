package com.example.masquesloyer;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class ThirdActivity extends AppCompatActivity {
    private EditText numero;
    private EditText rue;
    private EditText code;
    private EditText ville;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_third);

        Intent intention = getIntent();

        String numeroToUpdate = intention.getStringExtra("numero");
        String rueToUpdate = intention.getStringExtra("rue");
        String codeToUpdate = intention.getStringExtra("codePostal");
        String villeToUpdate = intention.getStringExtra("ville");

        numero = findViewById(R.id.numero_editText);
        rue = findViewById(R.id.rue_editText);
        code = findViewById(R.id.code_postal_editText);
        ville = findViewById(R.id.ville_editText);

        if (numeroToUpdate.length()==0)numero.setText("saisie vide");
        else numero.setText(numeroToUpdate);

        if (rueToUpdate.length()==0)rue.setText("saisie vide");
        else rue.setText(rueToUpdate);

        if (codeToUpdate.length()==0)code.setText("saisie vide");
        else code.setText(codeToUpdate);

        if (villeToUpdate.length()==0)ville.setText("saisie vide");
        else ville.setText(villeToUpdate);

    }

    public void cancel(View view){
        Intent resultat = new Intent();
        setResult(RESULT_CANCELED,resultat);
        finish();
    }

    public void validate(View view){
        Intent resultat = new Intent();

        resultat.putExtra("numero",numero.getText().toString());
        resultat.putExtra("rue",rue.getText().toString());
        resultat.putExtra("code",code.getText().toString());
        resultat.putExtra("ville",ville.getText().toString());

        setResult(RESULT_OK,resultat);
        finish();
    }

}