package com.example.testintent02;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void sendText(View maVue){
        TextView textView = findViewById(R.id.textToSend);
        String text = textView.getText().toString();

        Intent intention = new Intent(this,SecondActivity.class);
        intention.putExtra("texteSaisi",text);
        startActivityForResult(intention,   1);
    }

    public void updateText(View maVue){
        TextView textModifiedProposed = findViewById(R.id.texte_modifie);
        TextView textBase = findViewById(R.id.textToSend);
        textBase.setText(textModifiedProposed.getText().toString());

        textModifiedProposed.setText(""); // on remet à zero
    }
    protected void onActivityResult(int RequestCode,int ResultCode,Intent data) {

        super.onActivityResult(RequestCode, ResultCode, data);
        if (RequestCode == 1) {
            if (data.hasExtra("textUpdated")) {
                TextView textView = findViewById(R.id.texte_modifie);
                textView.setText(data.getStringExtra("textUpdated"));
            }
        }
    }
}

