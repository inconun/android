package com.example.testintent02;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import java.util.Locale;

public class SecondActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        Intent intention = getIntent();
        String text = intention.getStringExtra("texteSaisi");

        EditText editText = findViewById(R.id.textReceived);
        if (text.length()==0)editText.setText("saisie vide");
        else editText.setText(text);

    }

    public void majusculeText(View maVue){
        EditText editText = findViewById(R.id.textReceived);
        String textUpdated = editText.getText().toString();
        textUpdated = textUpdated.toUpperCase();

        sendIntentResult(textUpdated);

    }

    public void reverseText(View maVue){
        EditText editText = findViewById(R.id.textReceived);
        String textUpdated = editText.getText().toString();
        textUpdated = new StringBuilder(textUpdated).reverse().toString();

        sendIntentResult(textUpdated);

    }

    public void sendIntentResult(String textUpdated){
        Intent resultat = new Intent();
        resultat.putExtra("textUpdated",textUpdated);
        setResult(RESULT_OK,resultat);
        finish();
    }
}