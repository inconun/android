package com.example.projetloyer;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class SettingsActivity extends AppCompatActivity {

    private TextView comparisonSpeed; // l'affichage de la vitesse de comparaison
    private TextView characterNumber; // l'affichage du nombre de caractères voulus

    /**
     * Méthode appelée à la création de l'activité
     * Elle va notamment afficher les valeurs des paramètres que l'on veut modifier
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        comparisonSpeed = findViewById(R.id.comparisonSpeedTextView);
        characterNumber = findViewById(R.id.characterNumberTextView);

        Intent intent = getIntent();
        comparisonSpeed.setText(String.valueOf(intent.getIntExtra("comparisonSpeed",100)));
        characterNumber.setText(String.valueOf(intent.getIntExtra("characterNumber",0)));
    }

    /**
     * Méthode permettant d'augmenter et diminuer le paramètre de vitesse de comparaison
     * @param view
     */
    public void updateComparisonSpeed(View view) {
        String comparisonText = this.comparisonSpeed.getText().toString();
        int speed = Integer.parseInt(comparisonText);

        switch (view.getId()){
            case R.id.increaseComparisonSpeedButton:
                comparisonSpeed.setText(Integer.toString(speed+100));
                break;
            case R.id.decreaseComparisonSpeedButton:
                if(speed-100>=0) comparisonSpeed.setText(Integer.toString(speed-100));
                else comparisonSpeed.setText(Integer.toString(0));

        }
    }

    /**
     * Méthode permettant d'augmenter et diminuer le paramètre du nombre de caractères
     * @param view
     */
    public void updateCharacterNumberOfPalindrome(View view) {
        String characterNumberText = this.characterNumber.getText().toString();
        int numberOfcharacter = Integer.parseInt(characterNumberText);

        switch (view.getId()){
            case R.id.increaseCharacterNumberButton:
                characterNumber.setText(Integer.toString(numberOfcharacter+1));
                break;
            case R.id.decreaseCharacterNumberButton:
                if(numberOfcharacter-1>=0) characterNumber.setText(Integer.toString(numberOfcharacter-1));
                else characterNumber.setText(Integer.toString(0));

        }
    }

    /**
     * Méthode permettant de valider les paramètres fonctionnels à l'application
     * @param view
     */
    public void validateSettings(View view) {
            String characterNumberText = this.characterNumber.getText().toString();
            String comparisonSpeed = this.comparisonSpeed.getText().toString();

            Intent resultat = new Intent();
            resultat.putExtra("characterNumber",characterNumberText);
            resultat.putExtra("comparisonSpeed",comparisonSpeed);
            setResult(RESULT_OK,resultat);
            finish();
    }
}