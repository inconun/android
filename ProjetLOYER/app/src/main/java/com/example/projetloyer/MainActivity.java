package com.example.projetloyer;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.AssetManager;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.os.SystemClock;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.BackgroundColorSpan;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.Normalizer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


public class MainActivity extends AppCompatActivity {

    private EditText palindromeEditText; // l'editText du palindrome à comparer
    private TextView cleanedView; // le text view du palindrome nettoyé
    private TextView returnedView; // le text view du palindrome retourné
    private ProgressBar progressBar; // la barre de progression de la comparaison

    private String currentPalindrome; // on garde en mémoire le dernier palindrome confirmé
    private String currentSentence; // on garde en mémoire la dernière phrase confirmée

    private Menu optionMenu; // le menu d'option

    private Dialog aboutPopup; // popup affichage du menu about
    private AlertDialog alertDialogAdminForm; // popup de connexion


    private int COMPARISON_SPEED; // vitesse de comparaison des caractères (=100 par défaut)

    private int CHARACTER_NUMBER; // nombre de caractère voulus pour les phrases aléatoires ( 0 = peu importe )

    private List<String> palindromesList; // liste des palindromes du fichier de la mémoire interne
    private List<String> randomSentencesList; // liste des phrases aléatoires du fichier de la mémoire interne

    private List<String> filteredRandomSentencesList; // liste des palindromes d'un certain nombre de lettre (en fonction de CHARACTER_NUMBER)
    private List<String> filteredPalindromesList; // liste des phrases aléatoires d'un certain nombre de lettre (en fonction de CHARACTER_NUMBER)

    private final static String palindromesFile = "palindromesList.txt"; // Fichier des palindromes
    private final static String sentencesFile = "sentencesList.txt"; // Fichier des phrases aléatoires

    private AlertDialog.Builder builder; //  le builder pour créer la fenêtre de connexion du mode admin

    private final static String PASSWORD_ADMIN = "Perec"; // la constante mot de passe admin

    /**
     * Permet d'initialiser les listes, les constantes, les variables globales, rempli les listes, crée les fichiers,...
     * @param savedInstanceState
     */
    @SuppressLint("ResourceAsColor")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // initialisation constantes
        COMPARISON_SPEED = 100; //100 ms
        CHARACTER_NUMBER = 0; // pas de taille spécifique

        palindromeEditText = findViewById(R.id.palindromeEditText);
        cleanedView = findViewById(R.id.nettoyeeView);
        returnedView = findViewById(R.id.retourneeView);
        progressBar = findViewById(R.id.divider);
        progressBar.getProgressDrawable().setColorFilter(Color.MAGENTA, PorterDuff.Mode.MULTIPLY);

        //popup about
        aboutPopup = new Dialog(this);
        aboutPopup.setContentView(R.layout.popup_about);

        //initialisation  listes
        palindromesList = new ArrayList();
        randomSentencesList = new ArrayList();
        filteredPalindromesList = new ArrayList();
        filteredRandomSentencesList = new ArrayList();

        File palindromeFile = new File(getApplicationContext().getFilesDir(), palindromesFile);
        File randomSentencesFile = new File(getApplicationContext().getFilesDir(), sentencesFile);

        // en supposant que si l'un des fichiers existe, l'autre aussi (pas de suppression d'un fichier dans la mémoire)
        if (!palindromeFile.exists() || !randomSentencesFile.exists()) {
            createFilesAndImportSentences(); // on créé les fichiers dans la mémoire interne
        }

        // Remplissage des listes
        readData(palindromesList, palindromesFile);
        readData(randomSentencesList, sentencesFile);
        fillFilteredList(palindromesList, filteredPalindromesList, CHARACTER_NUMBER);
        fillFilteredList(randomSentencesList, filteredRandomSentencesList, CHARACTER_NUMBER);

        // création popup connexion admin
        makeAdminPopup();
    }

    /**
     * Rempli les listes filtrées (en fonction du nombre de caractères)
     * @param from la liste de base
     * @param to la liste filtrée
     * @param numberCharacter le nombre de caractères max
     */
    public void fillFilteredList(List<String> from, List<String> to, int numberCharacter) {
        String currentSentence;
        String filteredCurrentSentence;
        to.clear(); // vider la liste pour la re remplir
        for (int i = 0; i < from.size(); i++) {
            currentSentence = from.get(i);
            if (numberCharacter == 0) to.add(currentSentence); // si 0 alors pas de limite, on ajoute tout
            else {
                // on nettoie la chaine pour savoir sa taille
                filteredCurrentSentence = Normalizer.normalize(currentSentence, Normalizer.Form.NFD);
                filteredCurrentSentence = filteredCurrentSentence.replaceAll("[\\p{InCombiningDiacriticalMarks}]", "");
                filteredCurrentSentence = filteredCurrentSentence.replaceAll("[^\\w\\s]", "");
                filteredCurrentSentence = filteredCurrentSentence.replaceAll(" ", "");

                if (filteredCurrentSentence.length() == numberCharacter) to.add(currentSentence);
            }
        }
    }

    /**
     * Créé la fenêtre popup de connexion pour le mode Admin
     * Affiche et cache les menus liés au mode admin
     */
    public void makeAdminPopup() {
        builder = new AlertDialog.Builder(this);
        // Get the layout inflater
        LayoutInflater inflater = getLayoutInflater();
        // Inflate and set the layout for the dialog
        // Pass null as the parent view because its going in the dialog layout
        builder.setView(inflater.inflate(R.layout.popup_admin_connexion, null))
                // Add action buttons
                .setPositiveButton("Valider", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        Dialog dialogObj = (Dialog) dialog;
                        EditText passwordEditText = dialogObj.findViewById(R.id.passwordAdmin);
                        String password = passwordEditText.getText().toString();
                        if (password.equals(PASSWORD_ADMIN)) {
                            Toast.makeText(getApplicationContext(), "Connexion réussie", Toast.LENGTH_LONG).show();
                            optionMenu.findItem(R.id.connectAdmin).setVisible(false); // cache le bouton de connexion
                            optionMenu.findItem(R.id.adminMenu).setVisible(true); // affiche le menu de gestion du mode admin, avec deconnexion et ajout de palindromes

                        } else {
                            Toast.makeText(getApplicationContext(), "Mot de passe incorrect", Toast.LENGTH_LONG).show();

                        }
                        passwordEditText.setText("");
                    }
                });
        builder.create();
    }

    /**
     * Crée les fichiers dans la mémoire interne à partir des fichiers dans le dossier assets
     * Ajoute les palindromes dans le fichier interne palindrome ainsi que le fichier interne phrases
     * et les phrases non palindromes dans le fichier interne phrases
     */
    public void createFilesAndImportSentences() {
        // Open Stream to write file.
        try {
            FileOutputStream outPalindromes = this.openFileOutput(palindromesFile, MODE_APPEND);
            FileOutputStream outSentences = this.openFileOutput(sentencesFile, MODE_APPEND);
            AssetManager am = this.getAssets();

            // on créé le fichier palindromesList
            InputStream is = am.open("palindromes.txt");
            BufferedReader reader = new BufferedReader(new InputStreamReader(is, "windows-1252"));

            String line;
            //on copie le fichier d'assets dans le fichier mémoire interne
            while (reader.ready()) {
                line = reader.readLine();
                outPalindromes.write(line.getBytes());
                outPalindromes.write(10); // passer une ligne , 10 c'est un saut de ligne en ASCII

                outSentences.write(line.getBytes());
                outSentences.write(10);
            }
            outPalindromes.close();

            // on créé le fichier sentencesList.txt

            is = am.open("sentences.txt");
            reader = new BufferedReader(new InputStreamReader(is, "windows-1252"));

            //on copie le fichier d'assets dans le fichier mémoire interne
            while (reader.ready()) {
                outSentences.write(reader.readLine().getBytes());
                outSentences.write(10); // passer une ligne , 10 c'est un saut de ligne en ASCII
            }
            outSentences.close();

            // Toast.makeText(this,"FICHIER CREE AVEC SUCCES",Toast.LENGTH_LONG).show();

        } catch (Exception e) {
            System.out.println(e);
        }
    }

    /**
     * Récupère les phrases d'un fichier pour les stocker dans une liste
     * @param myList La liste dans laquelle stocker les phrases
     * @param simpleFileName Le fichier à partir duquel prendre les phrases
     */
    public void readData(List myList, String simpleFileName) {
        try {
            // Open stream to read file.
            FileInputStream in = this.openFileInput(simpleFileName);

            BufferedReader br = new BufferedReader(new InputStreamReader(in));

            String s;
            while ((s = br.readLine()) != null) {
                myList.add(s);
            }
            //Toast.makeText(this,"FICHIER IMPORTE AVEC SUCCES",Toast.LENGTH_LONG).show();

        } catch (Exception e) {
            Toast.makeText(this, "Error:" + e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Crée le menu d'option
     * @param menu
     * @return true
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        optionMenu = menu;
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_activity_menu, optionMenu);
        return true;
    }

    /**
     * Nettoie le palindrome, en retirant les accents,virgules,ponctuations,...
     * @param view
     */
    public void cleanPalindrome(View view) {
        String s = palindromeEditText.getText().toString();
        s = Normalizer.normalize(s, Normalizer.Form.NFD);
        s = s.replaceAll("[\\p{InCombiningDiacriticalMarks}]", "");
        s = s.replaceAll("[^\\w\\s]", "");
        s = s.replaceAll(" ", "");
        cleanedView.setText(s.toLowerCase());
    }

    /**
     * Retourne la chaine de caractère si elle a été nettoyée
     * @param view
     */
    public void returnPalindrome(View view) {

        String s = cleanedView.getText().toString();
        if (s.isEmpty()) {
            Toast toast = Toast.makeText(this, "Veuillez nettoyer le texte", Toast.LENGTH_SHORT);
            toast.show();
        } else {
            StringBuilder str = new StringBuilder(s);
            s = str.reverse().toString();
            returnedView.setText(s);
        }
    }

    /**
     * Cette méthode va permettre de vérifier qu'une phrase est un palindrome
     * Un thread va tester l'égalité de chaque caractère des chaines néttoyées et retournées et les mettre en couleur verte pour confirmer
     * Lorsqu'un caractère ne correspond pas avec la chaine voisine, la comparaison s'arrête et le mot n'est pas qualifié de palindrome
     * Une barre de progression est mise à jour en fonction de l'avancée de la comparaison
     * @param view
     */
    @SuppressLint("ResourceAsColor")
    public void comparePalindrome(View view) {
        currentSentence = palindromeEditText.getText().toString();// on retient la dernière phrase testée si on veut l'ajouter au fichier de phrases aléatoires
        String cleaned = cleanedView.getText().toString();
        String returned = returnedView.getText().toString();
        if (cleaned.isEmpty()) {
            Toast toast = Toast.makeText(this, "Veuillez nettoyer le texte", Toast.LENGTH_SHORT);
            toast.show();
        } else if (returned.isEmpty()) {
            Toast toast = Toast.makeText(this, "Veuillez retourner le texte", Toast.LENGTH_SHORT);
            toast.show();
        } else {
            progressBar.getProgressDrawable().setColorFilter(Color.MAGENTA, PorterDuff.Mode.MULTIPLY);
            SpannableString cleanedSpan = new SpannableString(cleaned);
            SpannableString returnedSpan = new SpannableString(returned);
            new Thread(new Runnable() {
                @Override
                public void run() {
                    int i;
                    for (i = 0; i < cleaned.length(); i++) {
                        int finalI = i;
                        if (cleaned.charAt(i) == returned.charAt(i)) { // si les lettres correspondent
                            cleanedView.post(() -> {
                                // on met la couleur verte sur les TextView
                                setSpan(Color.GREEN, cleanedView, cleanedSpan, finalI, finalI + 1);
                                setSpan(Color.GREEN, returnedView, returnedSpan, finalI, finalI + 1);
                                // On met à jour la progressBar
                                progressBar.setProgress(((finalI + 1) * (100 / cleaned.length())));
                                if (finalI + 1 == cleaned.length())
                                    progressBar.setProgress(progressBar.getMax());

                            });
                        } else { // si il y a une différence
                            cleanedView.post(new Runnable() {
                                @Override
                                public void run() {
                                    // On met la couleur rouge
                                    setSpan(Color.RED, returnedView, returnedSpan, finalI, finalI + 1);
                                    setSpan(Color.RED, cleanedView, cleanedSpan, finalI, finalI + 1);
                                    progressBar.setProgress((finalI + 1) * (100 / cleaned.length()));
                                    progressBar.getProgressDrawable().setColorFilter(Color.RED, PorterDuff.Mode.MULTIPLY);
                                }
                            });
                            break; // on sort donc car ce n'est pas un palindrome
                        }
                        SystemClock.sleep(COMPARISON_SPEED); // temps d'attente entre chaque comparaison de caractères

                    }
                    if (i == cleaned.length())
                        currentPalindrome = palindromeEditText.getText().toString(); // on retient le dernier palindrome testé pour potentiellement l'ajouter au fichier
                }
            }).start();

        }

    }

    /**
     * Permet de mettre les couleurs sur les TextView des chaînes nettoyées et retournées
     * @param color La couleur à appliquer
     * @param textView Le TextView à modifier
     * @param spannableString le string spannable
     * @param startIndex l'index de départ de la chaîne
     * @param endIndex l'index de fin de la chaîne
     */
    public void setSpan(int color, TextView textView, SpannableString spannableString, int startIndex, int endIndex) {
        spannableString.setSpan(new BackgroundColorSpan(color), startIndex, endIndex, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        textView.setText(spannableString, TextView.BufferType.SPANNABLE);
    }



    /// PARTIE DES FONCTIONS LIEES AU MENU

    /**
     * Selectionne une phrase aléatoire dans le fichier des phrases et la met dans l'EditText
     * @param menuItem
     */
    public void selectRandomSentence(MenuItem menuItem) {
        Collections.shuffle(filteredRandomSentencesList);
        if (filteredRandomSentencesList.size() > 0) {
            palindromeEditText.setText(filteredRandomSentencesList.get(0));
            cleanedView.setText("");
            returnedView.setText("");
        }
        else
            Toast.makeText(this, "Aucune phrase de " + CHARACTER_NUMBER + " caractères n'existe.", Toast.LENGTH_SHORT).show();
    }

    /**
     * Selectionne une phrase aléatoire dans le fichier palindromes et la met dans l'EditText
     * @param menuItem
     */
    public void selectRandomPalindrome(MenuItem menuItem) {
        Collections.shuffle(filteredPalindromesList);
        if (filteredPalindromesList.size() > 0) {
            palindromeEditText.setText(filteredPalindromesList.get(0));
            cleanedView.setText("");
            returnedView.setText("");
        }
        else
            Toast.makeText(this, "Aucun palindrome de " + CHARACTER_NUMBER + " caractères n'existe.", Toast.LENGTH_SHORT).show();
    }

    /**
     * Affiche la popup de l'option  "à propos" du menu
     * @param menuItem
     */
    public void about(MenuItem menuItem) {
        aboutPopup.show();
    }

    /**
     * Cache la popup de l'option "à propos" du menu
     * @param view
     */
    public void dismissPopup(View view) {
        aboutPopup.dismiss();
    }

    /**
     * Un mode de test (très ressemblant à la méthode comparePalindrome) permettant de tester si la chaîne est un palindrome ou non
     * Ce mode permet de parcourir la chaîne en partant de son début et de sa fin puis de tester caractère par caractère
     * jusqu'à arriver au centre de la chaîne
     * @param item
     */
    public void testMode(MenuItem item) {
        currentSentence = palindromeEditText.getText().toString();// on retient la dernière phrase testée si on veut l'ajouter au fichier de phrases aléatoires
        String cleaned = cleanedView.getText().toString();
        if (cleaned.isEmpty()) {
            Toast toast = Toast.makeText(this, "Vous devez nettoyer la phrase pour utiliser ce mode", Toast.LENGTH_SHORT);
            toast.show();
        } else {
            progressBar.getProgressDrawable().setColorFilter(Color.MAGENTA, PorterDuff.Mode.MULTIPLY);
            SpannableString cleanedSpan = new SpannableString(cleaned);
            new Thread(new Runnable() {
                @Override
                public void run() {
                    for (int i = 0; i < cleaned.length(); i++) {
                        if (cleaned.charAt(i) == cleaned.charAt(cleaned.length() - i - 1)) { // si les lettres correspondent
                            int finalI = i;
                            int finalEndI = cleaned.length() - i - 1;
                            cleanedView.post(() -> {
                                // on met la couleur verte sur les TextView
                                setSpan(Color.GREEN, cleanedView, cleanedSpan, finalI, finalI + 1);
                                setSpan(Color.GREEN, cleanedView, cleanedSpan, finalEndI, finalEndI + 1);
                                progressBar.setProgress(2 * ((finalI + 1) * (100 / cleaned.length())));
                            });
                            // On met à jour la progressBar
                            if (finalI == cleaned.length() / 2) {
                                progressBar.setProgress(progressBar.getMax());
                                currentPalindrome =  palindromeEditText.getText().toString(); // on retiens le dernier palindrome testé pour potentiellement l'ajouter au fichier
                                break;
                            }
                        } else { // si il y a une différence
                            int finalI = i;
                            int finalEndI = cleaned.length() - i - 1;
                            cleanedView.post(new Runnable() {
                                @Override
                                public void run() {
                                    // On met la couleur rouge
                                    setSpan(Color.RED, cleanedView, cleanedSpan, finalEndI, finalEndI + 1);
                                    setSpan(Color.RED, cleanedView, cleanedSpan, finalI, finalI + 1);
                                    progressBar.setProgress((finalI + 1) * (100 / cleaned.length()));
                                    progressBar.getProgressDrawable().setColorFilter(Color.RED, PorterDuff.Mode.MULTIPLY);
                                }
                            });
                            break; // on sort donc car ce n'est pas un palindrome
                        }
                        SystemClock.sleep(COMPARISON_SPEED); // temps d'attente entre chaque comparaison de caractères
                    }
                }
            }).start();
        }
    }


    /**
     * Permet d'ajouter une phrase passée en paramètre au fichier interne correspondant de palindromes
     * @param currentSentence La phrase à ajouter
     * @param file Le fichier dans lequel ajouter la phrase
     * @param list La liste dynamique reliée au fichier
     * @param filteredList La liste filtrée correspondante à la liste du fichier
     * @param isPalindrome Si la phrase passée est un palindrome ou non
     */
    public void addCurrentSentence(String currentSentence,String file,List<String> list, List<String> filteredList, Boolean isPalindrome) {

        if (currentSentence != null) {
            try {
                FileOutputStream out = this.openFileOutput(file, MODE_APPEND);
                //on copie le palindrome dans le fichier mémoire interne
                out.write(currentSentence.getBytes());
                out.write(10); // passer une ligne , 10 c'est un saut de ligne en ASCI
                out.close();
            } catch (Exception e) {
                System.out.println(e);
            }
            Toast.makeText(this, "Phrase ajoutée au fichier : " + currentSentence, Toast.LENGTH_SHORT).show();

            if (isPalindrome)this.currentPalindrome = null;
            else this.currentSentence =null;

            fillFilteredList(list, filteredList, CHARACTER_NUMBER);
        } else
            Toast.makeText(this, "Aucune nouvelle phrase à ajouter ou précédente deja ajoutée.", Toast.LENGTH_SHORT).show();

    }

    /**
     * Permet d'ajouter une phrase (après test de celle-ci) au fichier interne de phrases pré-définies
     * @param item
     */
    public void addSentenceToFile(MenuItem item) {
        switch (item.getItemId()){
            case R.id.addPalindromeToFile:
                addCurrentSentence(this.currentPalindrome,palindromesFile,palindromesList,filteredPalindromesList,true);
                break;
            case R.id.addSentenceToFile:
                addCurrentSentence(this.currentSentence,sentencesFile,randomSentencesList,filteredRandomSentencesList,false);
                break;
        }
    }


    /**
     * Permet d'afficher la popup de connexion du mode admin
     * @param item
     */
    public void adminMode(MenuItem item) {
        if (alertDialogAdminForm == null) alertDialogAdminForm = builder.show();
        alertDialogAdminForm.show();

    }

    /**
     * Permet de cacher le menu de gestion et d'afficher le menu de connexion du mode admin à la déconnexion
     * @param item
     */
    public void leaveAdminMode(MenuItem item) {
        optionMenu.findItem(R.id.connectAdmin).setVisible(true);
        optionMenu.findItem(R.id.adminMenu).setVisible(false);
    }

    /**
     * Permet d'acceder aux paramètres de l'application, dans une nouvelle activité
     * @param item
     */
    public void openSettings(MenuItem item) {
        Intent intent = new Intent(this, SettingsActivity.class);
        intent.putExtra("characterNumber", CHARACTER_NUMBER);
        intent.putExtra("comparisonSpeed", COMPARISON_SPEED);
        startActivityForResult(intent, 1);
    }

    /**
     * Méthode de retour des activités lancées :
     * Elle permet de mettre à jour les constantes en fonction des données récupérées du menu d'options
     * @param RequestCode Code de résultat de l'activité
     * @param ResultCode Code de résultat identifiant quelle activité a déclenché la méthode
     * @param data Les données de l"activité à récupérer
     */
    protected void onActivityResult(int RequestCode, int ResultCode, Intent data) {

        super.onActivityResult(RequestCode, ResultCode, data);
        if (ResultCode != RESULT_CANCELED) {
            if (RequestCode == 1) {
                if (data.hasExtra("comparisonSpeed")) {
                    String comparisonSpeed = data.getStringExtra("comparisonSpeed");
                    COMPARISON_SPEED = Integer.parseInt(comparisonSpeed);
                }
                if (data.hasExtra("characterNumber")) {
                    String characterNumber = data.getStringExtra("characterNumber");
                    CHARACTER_NUMBER = Integer.parseInt(characterNumber);
                    fillFilteredList(palindromesList, filteredPalindromesList, CHARACTER_NUMBER);
                    fillFilteredList(randomSentencesList, filteredRandomSentencesList, CHARACTER_NUMBER);
                }
            }
        }
    }
}