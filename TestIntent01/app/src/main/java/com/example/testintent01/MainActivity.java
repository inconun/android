package com.example.testintent01;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void sendText(View maVue){
        EditText editText = findViewById(R.id.textToSend);
        String text = editText.getText().toString();
        System.out.println(text);

        Intent intention = new Intent(this,SecondActivity.class);
        intention.putExtra("texteSaisi",text);
        startActivity(intention);
    }
}