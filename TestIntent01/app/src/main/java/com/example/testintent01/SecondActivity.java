package com.example.testintent01;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

public class SecondActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        Intent intention = getIntent();
        String text = intention.getStringExtra("texteSaisi");

        TextView textView = findViewById(R.id.textReceived);
        if (text.length()==0)textView.setText("saisie vide");
        else textView.setText(text);

        System.out.println("arrivé dans la seconde activité");
        System.out.println("voici le contenu : "+ text);
    }
}