package com.example.todoloyer;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import com.example.todoloyer.databinding.ActivityMainBinding;

import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckedTextView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private ActivityMainBinding binding;
    private ListView listView;
    private List<String> listViewData;
    private ArrayAdapter<String> adapter;
    private AlertDialog addTaskPopup;
    // fichier mémoire interne
    private final String simpleFileName = "todoListTpMaster2.txt";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        setSupportActionBar(binding.toolbar);

        listView = findViewById(R.id.listView);

        listView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);


        listViewData = new ArrayList<>();

        adapter = new ArrayAdapter<>(this,
                android.R.layout.simple_list_item_checked, listViewData);

        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                CheckedTextView v = (CheckedTextView) view;
                boolean currentCheck = v.isChecked();
                listView.setItemChecked(position,currentCheck);
            }
        });

        File file = new File(getApplicationContext().getFilesDir(),simpleFileName);
        if(file.exists()) {
            readData();
        }

        // popup dialog pour ajouter tache
        addTaskPopup  = makeAdminPopup();

    }
    public AlertDialog makeAdminPopup(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        // Get the layout inflater
        LayoutInflater inflater = getLayoutInflater();
        // Inflate and set the layout for the dialog
        // Pass null as the parent view because its going in the dialog layout
        builder.setView(inflater.inflate(R.layout.add_task_popup, null))
                // Add action buttons
                .setPositiveButton("Valider", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        Dialog dialogObj = (Dialog) dialog;
                        EditText taskAdded = dialogObj.findViewById(R.id.addTaskPopupEditText);
                        updateListView(taskAdded.getText().toString());
                        taskAdded.setText("");

                    }
                });
        return builder.create();
    }

    public void updateListView(String task){
        if(!task.isEmpty()) {
            // Ce bloc est en commentaire pour permettre la sauvegarde des tâches manuelle
            // si on l'active, chaque tâche ajoutée sera aussitôt ajoutée au fichier de la mémoire interne
            /*
            try {
                // Open Stream to write file.
                FileOutputStream out = this.openFileOutput(simpleFileName, MODE_APPEND);
                // Ghi dữ liệu.
                out.write(task.getBytes());
                out.write(10); // passer une ligne , 10 c'est un saut de ligne en ASCII
                out.close();
                Toast.makeText(this,"Task added : "+ task,Toast.LENGTH_SHORT).show();
            } catch (Exception e) {
                Toast.makeText(this,"Error:"+ e.getMessage(),Toast.LENGTH_SHORT).show();
            }
            */
            this.listViewData.add(task);
            this.adapter.notifyDataSetChanged();
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        switch (id){
            case R.id.cleanSelected:
                ArrayList<String> itemsToDelete = new ArrayList<>();
                SparseBooleanArray checkeditems = this.listView.getCheckedItemPositions();
                // on parcoure la listView
                for (int i=0;i<this.listView.getCount();i++){
                    if(checkeditems.get(i)){ // si l'item est coché
                        itemsToDelete.add(adapter.getItem(i)); // on l'ajoute dans une liste temporaire pour pas supprimer pendant le parcours
                    }
                }
                for(int i = 0; i < itemsToDelete.size(); i++) {
                    this.listViewData.remove(itemsToDelete.get(i)); //on supprime maintenant les items de la listData

                }
                this.listView.clearChoices();
                //for(int i=0;i<this.listView.getCount();i++) this.listView.setItemChecked(i,false);
                this.adapter.notifyDataSetChanged();
                break;
            case R.id.saveList :
                refreshFile();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    public void refreshFile(){

        try {
            // on peut supprimer l'ancien pour le réécrire
           // new File(getApplicationContext().getFilesDir(),simpleFileName).delete();

            // mais je vais juste repasser sur le fichier en rajoutant chaque ligne qui doit être présente
            FileOutputStream out = this.openFileOutput(simpleFileName,0);
            // Ghi dữ liệu.
            for(int i=0;i<listViewData.size();i++) {
                out.write(listViewData.get(i).getBytes());
                out.write(10); // passer une ligne , 10 c'est un saut de ligne en ASCII
            }
            out.close();
            this.adapter.notifyDataSetChanged();
            //Toast.makeText(this,"upression ?" + supressed,Toast.LENGTH_SHORT).show();


        } catch (Exception e) {
            Toast.makeText(this,"Error:"+ e.getMessage(),Toast.LENGTH_SHORT).show();
        }
    }

    public void addTask(MenuItem item){
        Intent intention = new Intent(this,SecondActivity.class);
        startActivityForResult(intention,   1);

    }

    public void addTaskWithButton(View view){
        this.addTaskPopup.show();
    }

    protected void onActivityResult(int RequestCode,int ResultCode,Intent data) {

        super.onActivityResult(RequestCode, ResultCode, data);
        if(ResultCode != RESULT_CANCELED) {
            if (RequestCode == 1) {
                if (data.hasExtra("taskAdded")) {
                    String task = data.getStringExtra("taskAdded");
                    updateListView(task);
                }
            }
        }
    }

    public void readData(){
        try {
            // Open stream to read file.
            FileInputStream in = this.openFileInput(simpleFileName);

            BufferedReader br= new BufferedReader(new InputStreamReader(in));

            String s;
            while((s=br.readLine())!= null)  {
                this.listViewData.add(s);
            }
            this.adapter.notifyDataSetChanged();

        } catch (Exception e) {
            Toast.makeText(this,"Error:"+ e.getMessage(),Toast.LENGTH_SHORT).show();
        }
    }

}