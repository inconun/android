package com.example.todoloyer;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class SecondActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
    }

    public void validAddTask(View view){
        EditText editText = findViewById(R.id.addTaskEditTextText);
        String taskAdded = editText.getText().toString();

        Intent result = new Intent();
        result.putExtra("taskAdded",taskAdded);
        setResult(RESULT_OK,result);
        finish();
    }

    public void cancel(View view){
        Intent result = new Intent();
        setResult(RESULT_CANCELED,result);
        finish();
    }
}