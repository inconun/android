package com.example.contentproviderloyer;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class AddWord extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_word);
    }

    public void sendWord(View view) {
        EditText editText = findViewById(R.id.addWord_editText);
        String word = editText.getText().toString();

        Intent result = new Intent();
        result.putExtra("wordToAdd",word);
        setResult(RESULT_OK,result);
        finish();

    }
}