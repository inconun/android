package com.example.contentproviderloyer;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;


public class AddContact extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_contact);
    }

    public void sendContact(View view) {
        EditText nameContact = findViewById(R.id.addContactName_editText);
        EditText numberContact = findViewById(R.id.addContactNumber_editText);
        String contactName = nameContact.getText().toString();
        String contactNumber = numberContact.getText().toString();

        Intent result = new Intent();
        result.putExtra("contactNameToAdd",contactName);
        result.putExtra("contactNumberToAdd",contactNumber);
        setResult(RESULT_OK,result);
        finish();
    }
}