package com.example.contentproviderloyer;

import android.annotation.SuppressLint;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.provider.UserDictionary;
import android.view.View;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.Switch;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public class DictionaryActivity extends AppCompatActivity {

    private SimpleCursorAdapter mCursorAdapter;
    private Cursor mc;
    private ContentResolver resolver;
    private Switch aSwitch;
    private ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dictionnary_activity);

        setListview();
    }

    @SuppressLint("UseSwitchCompatOrMaterialCode")
    public void setListview() {

        listView = findViewById(R.id.listView);
        mCursorAdapter = new SimpleCursorAdapter(
                this,//The application's Context object
                android.R.layout.simple_list_item_1,//A layout in XML for one row in the ListView
                mc,//The result from the query
                new String[]{
                        UserDictionary.Words.WORD
                },//A string array of column names in the cursor
                new int[]{
                        android.R.id.text1
                }, //An integer array of view IDs in the row layout
                0);
        listView.setAdapter(mCursorAdapter);


        aSwitch = findViewById(R.id.tri_oder);
        aSwitch.setChecked(true);

        resolver = getContentResolver();
        mc = resolver.query(UserDictionary.Words.CONTENT_URI, null, null, null, UserDictionary.Words.WORD + " ASC");

        mCursorAdapter.changeCursor(mc);
    }

    public void changeState(View view) {
        String triOrder = (aSwitch.isChecked() ? "ASC" : "DESC");
        mc = resolver.query(UserDictionary.Words.CONTENT_URI, null, null, null, UserDictionary.Words.WORD + " " + triOrder);

        mCursorAdapter.changeCursor(mc);
    }

    public void addWord(View view) {
        Intent intent = new Intent(this, AddWord.class);
        startActivityForResult(intent, 0);
    }

    public void deleteWord(View view) {
        Intent intent = new Intent(this, DeleteWord.class);
        startActivityForResult(intent, 1);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != RESULT_CANCELED) {
            if (requestCode == 0) {
                if (data.hasExtra("wordToAdd")) {
                    String word = data.getStringExtra("wordToAdd");

                    Boolean exists = false;
                    mc.moveToPosition(-1);

                    while (mc.moveToNext())
                        if (mc.getString(mc.getColumnIndexOrThrow(UserDictionary.Words.WORD)).equals(word))
                            exists = true;

                    if (exists == false) {
                        ContentValues contentValues = new ContentValues();
                        contentValues.put("word", word);
                        resolver.insert(UserDictionary.Words.CONTENT_URI, contentValues);
                        changeState(aSwitch);
                    }
                }
            }
            if (requestCode == 1) {
                if (data.hasExtra("wordToDelete")) {
                    String word = data.getStringExtra("wordToDelete");

                    Boolean exists = false;
                    mc.moveToPosition(-1);

                    while (mc.moveToNext())
                        if (mc.getString(mc.getColumnIndexOrThrow(UserDictionary.Words.WORD)).equals(word))
                            exists = true;

                    if (exists == true) {
                        System.out.println("Suppression");
                        resolver.delete(UserDictionary.Words.CONTENT_URI, "word = ?", new String[]{word});
                        changeState(aSwitch);
                    }
                }
            }
        }

    }


}