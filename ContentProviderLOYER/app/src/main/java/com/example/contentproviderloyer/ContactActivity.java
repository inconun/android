package com.example.contentproviderloyer;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentProviderOperation;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Intent;
import android.content.OperationApplicationException;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.RemoteException;
import android.provider.ContactsContract;
import android.provider.UserDictionary;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Switch;

import com.example.contentproviderloyer.beans.Contact;

import java.util.ArrayList;

public class ContactActivity extends AppCompatActivity {

    private Switch aSwitch;
    private ListView listView;
    private ArrayList<Contact> contactList;
    private ArrayAdapter<Contact> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact);

        aSwitch = findViewById(R.id.contacts_tri_order);
        listView = findViewById(R.id.contacts_listView);

        aSwitch.setChecked(true);

        contactList = new ArrayList<>();
        adapter = new ContactAdapter(this, contactList);
        listView.setAdapter(adapter);
        printContacts();

    }

    public void printContacts() {
        contactList.clear();
        String triOrder = (aSwitch.isChecked() ? "ASC" : "DESC");
        Cursor c = getContentResolver().query(ContactsContract.Contacts.CONTENT_URI,
                null,
                null,
                null,
                ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME+" "+triOrder);


        while (c.moveToNext()) {

            String fullName = c.getString(c.getColumnIndexOrThrow(ContactsContract.Contacts.DISPLAY_NAME));
            int idIndex = c.getColumnIndex(ContactsContract.Contacts._ID);
            int idPhoneNumberCheck = c.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER);
            if (idIndex >= 0 && idPhoneNumberCheck >= 0) {
                String id = c.getString(idIndex);
                if (c.getInt(idPhoneNumberCheck) > 0) {
                    Cursor cursorInfo;
                    cursorInfo = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?", new String[]{id}, null);
                    while (cursorInfo.moveToNext()) {
                        int idPhoneNumber = cursorInfo.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
                        if (idPhoneNumber >= 0) {
                            String phoneNumber = cursorInfo.getString(idPhoneNumber);
                            System.out.println(phoneNumber + " " + fullName);
                            contactList.add(new Contact(fullName, phoneNumber));
                        }
                    }
                    cursorInfo.close();
                }
            }
        }
        c.close();
        adapter.notifyDataSetChanged();
    }

    private boolean containsSameContact(String phoneNumber) {
        for (int i=0;i<contactList.size();i++){
            System.out.println("numero testé : "+ contactList.get(i).getPhoneNumber());
            if (contactList.get(i).getPhoneNumber().equals(phoneNumber)) {
                return true;
            }
        }
        return false;
    }

    public void addContact(View view) {
        Intent intent = new Intent(this, AddContact.class);
        startActivityForResult(intent, 0);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != RESULT_CANCELED) {
            if (requestCode == 0) {
                if (data.hasExtra("contactNameToAdd") && data.hasExtra("contactNumberToAdd") ) {
                    String name = data.getStringExtra("contactNameToAdd");
                    String number = data.getStringExtra("contactNumberToAdd");

                    if (!containsSameContact(number))insertContact(getContentResolver(),name, number);
                    printContacts();
                }
            }
            if(requestCode==1){
                if (data.hasExtra("contactNumberToDelete")) {
                    String number = data.getStringExtra("contactNumberToDelete");
                    deleteContact(getContentResolver(), number);
                    printContacts();
                }
            }
        }

    }

    public static boolean insertContact(ContentResolver contactAdder, String firstName, String mobileNumber) {
        ArrayList<ContentProviderOperation> ops = new ArrayList<>();
        ops.add(ContentProviderOperation.newInsert(ContactsContract.RawContacts.CONTENT_URI).withValue(ContactsContract.RawContacts.ACCOUNT_TYPE, null).withValue(ContactsContract.RawContacts.ACCOUNT_NAME, null).build());

        ops.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI).withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0).withValue(ContactsContract.Data.MIMETYPE,ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE).withValue(ContactsContract.CommonDataKinds.StructuredName.GIVEN_NAME,firstName).build());

        ops.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI).withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0).withValue(ContactsContract.Data.MIMETYPE,ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE).withValue(ContactsContract.CommonDataKinds.Phone.NUMBER,mobileNumber).withValue(ContactsContract.CommonDataKinds.Phone.TYPE, ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE).build());
        try {
            contactAdder.applyBatch(ContactsContract.AUTHORITY, ops);
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    public void deleteContact(View view) {
        Intent intent = new Intent(this, DeleteContact.class);
        startActivityForResult(intent, 1);

    }

    public static void deleteContact(ContentResolver contactHelper, String number) {
        ArrayList<ContentProviderOperation> ops = new ArrayList<>();
        String[] args = new String[] { String.valueOf(getContactID(contactHelper, number)) };
        ops.add(ContentProviderOperation.newDelete(ContactsContract.RawContacts.CONTENT_URI).withSelection(ContactsContract.RawContacts.CONTACT_ID + "=?", args).build());
        try {
            contactHelper.applyBatch(ContactsContract.AUTHORITY, ops);
        } catch (RemoteException e) {
            e.printStackTrace();
        } catch (OperationApplicationException e) {
            e.printStackTrace();
        }
    }

    private static long getContactID(ContentResolver contactHelper,String number) {
        Uri contactUri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(number));
        String[] projection = { ContactsContract.PhoneLookup._ID };
        Cursor cursor = null;

        try {
            cursor = contactHelper.query(contactUri, projection, null, null,null);
            if (cursor.moveToFirst()) {
                int personID = cursor.getColumnIndex(ContactsContract.PhoneLookup._ID);
                return cursor.getLong(personID);
            }
            return -1;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
                cursor = null;
            }
        }
        return -1;
    }

    public void changeState(View view) {
        printContacts();
    }
}