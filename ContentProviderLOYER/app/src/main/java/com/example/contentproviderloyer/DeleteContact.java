package com.example.contentproviderloyer;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class DeleteContact extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_delete_contact);
    }

    public void sendContact(View view) {
        EditText editText = findViewById(R.id.deleteContact_editText);
        String contact = editText.getText().toString();

        Intent result = new Intent();
        result.putExtra("contactNumberToDelete",contact);
        setResult(RESULT_OK,result);
        finish();

    }
}