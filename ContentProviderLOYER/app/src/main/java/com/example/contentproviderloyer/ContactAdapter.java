package com.example.contentproviderloyer;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.contentproviderloyer.beans.Contact;

import java.util.ArrayList;

public class ContactAdapter extends ArrayAdapter<Contact> {


    public ContactAdapter(Context context, ArrayList<Contact> contacts) {
        super(context, 0, contacts);
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Récupération du contact concerné.
        Contact contact = getItem(position);
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(android.R.layout.simple_list_item_2, parent, false);
        }
        // Affichage des valeurs de contact.
        TextView fullName = (TextView) convertView.findViewById(android.R.id.text1);
        TextView phoneNumber = (TextView) convertView.findViewById(android.R.id.text2);
        fullName.setText(contact.getName());
        phoneNumber.setText(contact.getPhoneNumber());
        return convertView;
    }
}