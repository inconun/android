package com.example.contentproviderloyer;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class DeleteWord extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_delete_word);
    }

    public void sendWord(View view) {
        EditText editText = findViewById(R.id.deleteWord_editText);
        String word = editText.getText().toString();

        Intent result = new Intent();
        result.putExtra("wordToDelete",word);
        setResult(RESULT_OK,result);
        finish();

    }
}