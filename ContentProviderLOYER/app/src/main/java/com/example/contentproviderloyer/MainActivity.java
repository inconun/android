package com.example.contentproviderloyer;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    }

    public void goToDictionary(View view) {
        Intent intent = new Intent(this, DictionaryActivity.class);
        startActivityForResult(intent, 1);

    }

    public void goToContacts(View view) {
        Intent intent = new Intent(this, ContactActivity.class);
        startActivityForResult(intent, 2);

    }
}