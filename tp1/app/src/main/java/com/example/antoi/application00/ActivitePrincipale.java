package com.example.antoi.application00;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class ActivitePrincipale extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}
