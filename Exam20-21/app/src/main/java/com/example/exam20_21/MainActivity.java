package com.example.exam20_21;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private final int POWER_INIT = 100;
    private final int HP_INIT    = 10;
    private final int HP_LOOSE   = 3;
    private final int HP_FLEE    = 1;
    private final int POWER_WIN  = 10;

    private Map<String, Integer> rooms;
    private int[] buttons = {R.id.button01, R.id.button02, R.id.button03, R.id.button04, R.id.button05, R.id.button06, R.id.button07, R.id.button08, R.id.button09, R.id.button10, R.id.button11, R.id.button12, R.id.button13, R.id.button14, R.id.button15, R.id.button16};

    private TextView piecesLeft;
    private TextView powerValue;
    private TextView hpValue;
    private TextView resultText;
    private TextView resultValue;

    private int currentPower;
    private int currentHp;
    private int nbPiecesLeft;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        init();
    }

    private void init() {
        piecesLeft = (TextView) findViewById(R.id.pieces_left_value);
        powerValue      = (TextView) findViewById(R.id.power_value);
        hpValue         = (TextView) findViewById(R.id.hp_value);
        resultText      = (TextView) findViewById(R.id.result);
        resultValue     = (TextView) findViewById(R.id.result_value);

        powerValue.setText(String.valueOf(POWER_INIT));
        hpValue.setText(String.valueOf(HP_INIT));

        currentPower = POWER_INIT;
        currentHp    = HP_INIT;
        nbPiecesLeft = 16;

        rooms = new HashMap<>();

        int enemyPower;
        for (int i = 1; i <= 16; i++) {
            enemyPower = 1 + (int) (Math.random() * 149);
            rooms.put((i < 10 ? "0" : "") + i, enemyPower);
        }

        Button button;
        for (int i = 0; i < rooms.size(); i++) {
            button = (Button) findViewById(buttons[i]);
            button.setOnClickListener(this);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.retry:
                retry();
            case R.id.quit:
                quit();
        }
        return super.onOptionsItemSelected(item);
    }

    public void retry() {
        Intent intent = new Intent(this, MainActivity.class);
        this.startActivity(intent);
        this.finishAffinity();
    }

    public void quit() {
        finishAndRemoveTask();
        System.exit(0);
    }

    @Override
    public void onClick(View view) {
        Button b = (Button) view;
        Intent intent = new Intent(MainActivity.this, Fight.class);
        System.out.println(b.getText().toString());
        intent.putExtra("room", b.getText().toString());
        intent.putExtra("playerPower", currentPower);
        intent.putExtra("playerHp", currentHp);
        intent.putExtra("enemyPower", Integer.parseInt(String.valueOf(rooms.get(b.getText().toString()))));
        startActivityForResult(intent, 01);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 01 && resultCode == RESULT_OK) {
            // Combat terminé
            double res  = data.getDoubleExtra("result", 0);
            String room = data.getStringExtra("room");
            System.out.println("res : " + res);
            System.out.println("room : " + room);

            if (res < 0) {
                resultValue.setText(getString(R.string.result_defeat));
                currentHp -= HP_LOOSE;
                hpValue.setText(String.valueOf(currentHp));
                checkIfDead();
            } else {
                resultValue.setText(getString(R.string.result_victory));
                currentPower += POWER_WIN;
                majAfterWin(room);
                powerValue.setText(String.valueOf(currentPower));
                checkIfWin();
            }
        } else if (requestCode == 01 && resultCode == RESULT_CANCELED) {
            // Fuite ou Retour
            resultValue.setText(getString(R.string.result_fuite));
            currentHp -= HP_FLEE;
            hpValue.setText(String.valueOf(currentHp));
        }
    }

    private void majAfterWin(String room) {
        Button b = (Button) findViewById(buttons[Integer.parseInt(room) - 1]);
        b.setText("X");
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast toast = Toast.makeText(
                        getApplicationContext(),
                        getString(R.string.toast_enemy_dead),
                        Toast.LENGTH_SHORT
                );
                toast.show();
            }
        });
        nbPiecesLeft--;
        piecesLeft.setText(String.valueOf(nbPiecesLeft));
    }

    private void checkIfDead() {
        if (currentHp <= 0) {
            resultText.setText(getString(R.string.result_loose_head));
            resultValue.setText(getString(R.string.loose));
            gameEnd();
        }
    }

    private void checkIfWin() {
        if (nbPiecesLeft == 0) {
            resultValue.setText(getString(R.string.result_victory_head));
            resultText.setText(getString(R.string.win));
            gameEnd();
        }
    }

    private void gameEnd() {
        for (int i = 0; i < buttons.length; i++) {
            Button b = (Button) findViewById(buttons[i]);
            b.setClickable(false);
            b.setEnabled(false);
        }
    }
}