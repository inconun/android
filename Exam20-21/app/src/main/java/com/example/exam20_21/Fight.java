package com.example.exam20_21;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class Fight extends AppCompatActivity {

    int enemyPower, playerPower, playerHp;
    String room;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fight);

        TextView powerValue      = (TextView) findViewById(R.id.fight_power_value);
        TextView hpValue         = (TextView) findViewById(R.id.fight_hp_value);
        TextView enemyPowerValue = (TextView) findViewById(R.id.fight_enemy_power_value);
        TextView fightRoom       = (TextView) findViewById(R.id.fight_room);

        enemyPower = getIntent().getIntExtra("enemyPower", 0);
        playerPower = getIntent().getIntExtra("playerPower", 0);
        playerHp = getIntent().getIntExtra("playerHp", 0);
        room = getIntent().getStringExtra("room");

        powerValue.setText(String.valueOf(playerPower));
        hpValue.setText(String.valueOf(playerHp));
        enemyPowerValue.setText(String.valueOf(enemyPower));
        fightRoom.setText(fightRoom.getText().toString() + " " + room);
    }

    public void attack(View v) {
        Intent result = new Intent();
        double res = playerPower * Math.random() - enemyPower * Math.random();
        System.out.println("RESULTAT COMBAT : " + res);
        result.putExtra("result", res);
        result.putExtra("room", room);
        setResult(RESULT_OK, result);
        finish();
    }

    public void fuite(View v) {
        Intent result = new Intent();
        setResult(RESULT_CANCELED, result);
        finish();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent result = new Intent();
        setResult(RESULT_CANCELED, result);
        finish();
    }
}